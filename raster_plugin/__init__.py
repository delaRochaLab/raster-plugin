# !/usr/bin/python3
# -*- coding: utf-8 -*-

__version__ = "3.0"

from pyforms import conf

conf += 'raster_plugin.settings'
conf += 'raster_plugin.resources'


import loggingbootstrap

# setup different loggers but output to single file
loggingbootstrap.create_double_logger("raster_plugin", conf.APP_LOG_HANDLER_CONSOLE_LEVEL,
									  conf.APP_LOG_FILENAME,
									  conf.APP_LOG_HANDLER_FILE_LEVEL)
