#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os

SETTINGS_PRIORITY = 89

# THESE SETTINGS ARE NEEDED FOR PYSETTINGS
PYFORMS_USE_QT5 = True

SESSIONRASTER_PLUGIN_ICON = os.path.join(os.path.dirname(__file__), 'resources', 'raster.png')

SESSIONRASTER_PLUGIN_WINDOW_SIZE 	= 700, 600
SESSIONRASTER_PLUGIN_REFRESH_RATE 	= 3000 # milliseconds

