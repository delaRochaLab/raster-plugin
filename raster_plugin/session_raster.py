# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" session_window.py

"""

import logging

from pyforms import conf

import numpy as np

from AnyQt.QtCore import QTimer, QEventLoop, QCoreApplication
from AnyQt.QtWidgets import QMessageBox

from pybpodapi.session import Session
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlProgress
from pyforms.controls import ControlText
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel
from pyforms.controls import ControlButton
from pyforms.controls import ControlCheckBox
from pyforms.controls import ControlCombo

from raster_plugin.exc_manager import log
from raster_plugin.raster_data import RasterData

import matplotlib.gridspec as gridspec
from matplotlib.lines import Line2D
from matplotlib.ticker import MaxNLocator
from matplotlib.ticker import FormatStrFormatter

from types import SimpleNamespace

import logging
import traceback
import datetime

import os
import json

logger = logging.getLogger(__name__)


class SessionRaster(BaseWidget):
    """ Plugin main window """
    
    @log
    def __init__(self, session):
        BaseWidget.__init__(self, session.name)
        # ------ Var init: ---------------------
        self.layout().setContentsMargins(5, 5, 5, 5)  # Set graphic margin.
        self.session = session  # Session class of the pybpod to get data.
        self.LAST_POSITION = 0  # This variable records the last received dataframe index.
        self.trials_to_do = []
        # --------- Form creation: --------------
        self._graph = ControlMatplotlib('Graph')
        self._updatebutton = ControlButton('Update')
        self._axis_size = ControlText('Length: ', default='30', helptext='Number of trials to plot.')
        self._axis_start = ControlText('Start: ', default='0', helptext='In Range mode, select the first trial to plot', enabled=False)
        self._title_trend = ControlLabel('Raster visualization.')
        self._task_settings = ControlCombo('Presets: ', helptext='The settings for this session.')
        self._alignment = ControlCombo('Alignment: ', helptext='Change the state alignment.')
        self._show_outs = ControlCheckBox(' Show out pokes', helptext='Enable and disable pokes going out of the ports.')
        self._plot_mode = ControlCombo('Plot mode: ', helptext='Choose between different types of plotting intervals.')
        # Fill the modes list:
        for elem in ['Last', 'First', 'Range']:
            self._plot_mode.add_item(elem, elem)
        self._plot_mode.changed_event = self.refresh_mode
        # Set the task list:
        self.complete_settings = self.read_json_settings()
        for elem in self.complete_settings.keys():
            self._task_settings.add_item(elem, elem)
        # Fill the alignment list with state names from the settings file:
        self.refresh_alignment()
        # If the user changes the current task, we want to update the alignment options:
        self._task_settings.changed_event = self.refresh_alignment
        # Form position:
        self._formset = [
            '_title_trend', '=', ('_task_settings',  '||', '_alignment', '||',  '_show_outs',  '||',  '_updatebutton'),             ('_plot_mode', '_axis_start', '_axis_size', ' ', ' ', ' '),
            '=', '_graph',
        ]
        # Update button action:
        self._updatebutton.value = self.__updateAction
        # Set the timer to recall the function update
        self._timer = QTimer()
        self._timer.timeout.connect(self.update)
        # ----------- Info for the window title: ---------------------
        try:
            self._setup_name = self.session.data[self.session.data.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
        except IndexError:
            self._setup_name = "Unknown box"
        try:
            self._session_started = self.session.data[self.session.data.MSG == 'SESSION-STARTED']['+INFO'].iloc[0].split()[1][0:8]
        except IndexError:
            self._session_started = "Unknown time"
        try:
            self._subject_name = self.session.data[self.session.data.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
            self._subject_name = self._subject_name[1:-1].split(',')[0].strip("'")
        except IndexError:
            self._subject_name = "Unknown subject"
        try:
            self._protocol_name = self.session.data[self.session.data.MSG == 'PROTOCOL-NAME']['+INFO'].iloc[0]
        except IndexError:
            self._setup_name = "Unknown protocol"

        # Try to match the task name with a setting. If none of the names match,
        # pick the first setting by default.
        task_index = self._task_settings.get_item_index_by_name(self._protocol_name)
        if task_index != -1:
            self._task_settings.current_index = task_index
        self.current_task = self._task_settings.value
        # Obtain the full settings for the session, as a namespace (see the property below):
        current_settings = self.get_current_settings
        # Initialize the alignment control with the alignment specified in the settings:
        self.current_alignment = current_settings.alignment
        state_index = self._alignment.get_item_index_by_name(self.current_alignment)
        self._alignment.current_index = state_index
        # RasterData class that will make most of the preparations for the plots:
        self.processor = RasterData(self._axis_size.value, current_settings)
        # Obtain the legend for the selected settings:
        self.get_legend()
        # Define two plots with GridSpec, a narrow one for the current trial plot
        # and a wider one for the past trials visualization:
        self.figure = self._graph.fig
        gs = gridspec.GridSpec(5,1)
        self.axis1 = self.figure.add_subplot(gs[0,0])
        self.axis2 = self.figure.add_subplot(gs[1:, 0])
        box = self.axis2.get_position()
        self.axis2.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width, box.height * 0.9])
        box = self.axis1.get_position()
        self.axis1.set_position([box.x0, box.y0 + box.height * 0.8,
                 box.width, box.height])
        self.axis2.set_ylim([0,50])
        # Initialize the plot and set the title:
        self._graph.value = self.__on_draw
        self.title = f'{self._setup_name} / {self._session_started} / {self._subject_name}'

    @log
    def read_json_settings(self):
        """
        Read the JSON settings file and convert it to a namespace for easy access in the main RasterData 
        class.
        """
        path = os.path.join(conf.GENERIC_EDITOR_PLUGINS_PATH, 'raster-plugin', 'raster_plugin', 'plot_settings.json')
        with open(path) as settings_file:
            return json.load(settings_file)

    @log
    def refresh_mode(self):
        """
        Action tied to the plot mode ControlCombo "changed" event.
        """
        mode = self._plot_mode.value
        if mode == 'Range':
            self._axis_start.enabled = True
        else:
            self._axis_start.enabled = False

    @log
    def refresh_alignment(self):
        """
        Refresh the alignment ControCombo with state names after changing the presets:
        """
        self._alignment.clear()
        for state in self.get_current_settings.state_list:
            self._alignment.add_item(state, state)
        state_index = self._alignment.get_item_index_by_name(self.get_current_settings.alignment)
        self._alignment.current_index = state_index

    def __updateAction(self):
        """ 
        Action tied to the update button; the user should click after changing the number of trials on the plot
        or after selecting a different preset or alignment.
        """
        # Changing the settings implies having to recalculate everything, and it can be a bit slow.
        # Thus, make sure to only change the settings if the settings have changed (obvious, right?)
        if self.current_task != self._task_settings.value:
            self.processor.change_settings(self.get_current_settings)
            self.current_task = self._task_settings.value 
            self.LAST_POSITION = 0
            self.trials_to_do = []
            self.get_legend()
        if self.current_alignment != self._alignment.value:
            self.processor.change_alignment(self._alignment.value)
            self.current_alignment = self._alignment.value
            self.LAST_POSITION = 0
            self.trials_to_do = []
        # Check the axis length and start values and update the graph:
        try:
            length = int(self._axis_size.value)  
            start = int(self._axis_start.value)
            if length > 0 and start > -1:  # It's ok only in this case
                self.processor.axis_size = length
                self.processor.start = start
                self.processor.plot_mode = self._plot_mode.value
                self.read_message_queue()
                self._graph.draw()
            else:  # Otherwise show poupup message. 
                self.warning('Only positive integers are allowed.', 'ValueError')
        except ValueError:
            self.warning('Only positive integers are allowed.', 'ValueError')

    @log
    def read_message_queue(self):
        """ 
        Reads the new data coming from BPOD and gets the index of the past completed
        trials. Then sends the completed trials to RasterData for processing, and then the
        current trial separately.
        """
        try:
            dataframe = self.session.data[self.LAST_POSITION:]
            new_trials = dataframe.query("TYPE=='TRIAL' and MSG=='New trial'").index
            trials_to_do = np.concatenate((self.trials_to_do, new_trials)).astype(int)
            if trials_to_do.size > 1:
                start, end = trials_to_do[0], trials_to_do[-1]
                self.processor.process_past_trials(trials_to_do, self.session.data[start:end])
            if trials_to_do.size:  # To avoid errors at startup.
                last_trial = trials_to_do[-1]
                self.trials_to_do = [last_trial]
                self.processor.process_current_trial(self.session.data[last_trial:])
                self._graph.draw()
        # If there's an error in the plot, save the traceback into a file for later check.
        # Catching general Exceptions is bad, but in this case it's fine.
        except Exception as err:
            if hasattr(self, '_timer'):
                self._timer.stop()
            # logger.error(str(err), exc_info=True)
            traceback.print_tb(err.__traceback__)
            with open('plugins_logs.txt', 'a') as logfile:
                    logfile.write(str(datetime.datetime.now())+'\n')
                    logfile.write(traceback.format_exc())
                    logfile.write('-'*25)
            QMessageBox.critical(self, "Error",
                                 "An error in the raster plugin occurred. Check logs for details.")
        finally:
            self.LAST_POSITION += len(dataframe)

    @log
    def update(self):
        """
        This method is tied to a Qtimer that keeps calling it every X milliseconds,
        where X is a number defined in the settings. Its main job is to periodically 
        call the read_message_queue function, which will refresh the plot if necessary.
        """
        if not self.session.is_running: 
            self._timer.stop()
        if len(self.session.data) > self.LAST_POSITION:
            self.read_message_queue()

    @log
    def __on_draw(self, figure):
        """ Redraws the figure. """

        def get_markersize():
            """
            Custom markser sizes that depend on the axis, to avoid
            them being too big or too small.
            """
            axis = self.processor.axis_size
            if axis > 50:
                markersize = 5
            elif 10 < axis <= 50:
                markersize = int(self.processor.axis_size * -0.125 + 11.25)
            else:
                markersize = int(self.processor.axis_size * -0.5 + 15)
            return markersize

        # ---------- Get all the data from the processor class. ---------- 
        # Note the call to check_plot_limits(); it has to be done prior
        # to calling any of the data properties, or the axes limits will be wrong.
        self.processor.check_plot_limits()
        past_states = self.processor.past_states
        past_pokes = self.processor.past_pokes 
        current_states = self.processor.current_states
        current_pokes = self.processor.current_pokes
        markersize = get_markersize()
        # --------------- Gather and plot all the past states: ---------------------
        if not past_states.empty:
            if hasattr(self, 'bottom_bars'):
                self.bottom_bars.remove()
            self.bottom_bars = self.axis2.barh(past_states['yaxis'].astype(float),
                    past_states['length'].astype(float),
                    left=past_states['offset'].astype(float),
                    color=past_states['colors'], 
                    alpha=0.5, zorder=1)
            try:
                special_markers = self.get_current_settings.special_markers
                special_markers_loc = past_states.query("states in @special_markers")
                if not hasattr(self, 'special_marker_dots'):
                    self.special_marker_dots = self.axis2.scatter(special_markers_loc['offset'].astype(float),
                            special_markers_loc['yaxis'].astype(float),
                            color=special_markers_loc['colors'],
                            s=markersize, zorder=2, marker='x') 
                else:
                    self.special_marker_dots.set_offsets(np.c_[special_markers_loc['offset'].astype(float), special_markers_loc['yaxis'].astype(float)])
                    self.special_marker_dots.set_color(special_markers_loc['colors'])
            except AttributeError: # The user may not have specified special_markers, so check it.
                pass
        if not past_pokes.empty:
            pokes_in = past_pokes.query("pokes in @self.processor.st.pokes_in")
            if not hasattr(self, 'past_dots_in'):
                self.past_dots_in = self.axis2.scatter(pokes_in['offset'].astype(float),
                        pokes_in['yaxis'].astype(float),
                        color=pokes_in['colors'],
                        s=markersize, zorder=2,  
                        marker=self.get_current_settings.poke_markers['in']) 
            else:
                self.past_dots_in.set_offsets(np.c_[pokes_in['offset'], pokes_in['yaxis']])
                self.past_dots_in.set_color(pokes_in['colors'])
            if self._show_outs.value:  # If the checkbox is checked
                pokes_out = past_pokes.query("pokes in @self.processor.st.pokes_out")
                if not hasattr(self, 'past_dots_out'):
                    self.past_dots_out = self.axis2.scatter(pokes_out['offset'].astype(float),
                            pokes_out['yaxis'].astype(float), 
                            color=pokes_out['colors'],
                            s=markersize, zorder=2, 
                            marker=self.get_current_settings.poke_markers['out']) 
                else:
                    self.past_dots_out.set_offsets(np.c_[pokes_out['offset'], pokes_out['yaxis']])
                    self.past_dots_out.set_color(pokes_out['colors'])
        # --------- Bottom plot settings: ------------------
        if self.processor.axis_size > self.processor.PLOT_END:
            self.axis2.set_ylim([self.processor.PLOT_START, self.processor.axis_size])
        else:
            self.axis2.set_ylim([self.processor.PLOT_START, self.processor.PLOT_END+1])
        self.axis2.set_xlabel('Time [s]')
        self.axis2.set_ylabel('Trials')
        self.axis2.yaxis.set_major_locator(MaxNLocator(integer=True))
        self.axis2.set_xlim([-3,4]) 
        # ----------- Gather and plot current states and pokes: -------------
        if not current_states.empty:
            if hasattr(self, 'top_bars'):
                self.top_bars.remove()
            self.top_bars = self.axis1.barh(current_states['yaxis'].astype(float), 
                    current_states['length'].astype(float), 
                    left=current_states['start-times'].astype(float), 
                    color=current_states['colors'], 
                    alpha=0.6, zorder=1, height=2) 
        if not current_pokes.empty:
            c_pokes_in = current_pokes.query("pokes in @self.processor.st.pokes_in")
            if not hasattr(self, 'current_dots_in'):
                self.current_dots_in = self.axis1.scatter(c_pokes_in['start-times'].values.astype(float), 
                        c_pokes_in['yaxis'].values.astype(float), 
                        color=c_pokes_in['colors'].values,
                        s=8, zorder=2, 
                        marker=self.get_current_settings.poke_markers['in']) 
            else:
                self.current_dots_in.set_offsets(np.c_[c_pokes_in['start-times'].values.astype(float), c_pokes_in['yaxis'].values.astype(float)])
                self.current_dots_in.set_color(c_pokes_in['colors'].values)
            if self._show_outs.value:  # If the checkbox is checked
                c_pokes_out = current_pokes.query("pokes in @self.processor.st.pokes_out")
                if not hasattr(self, 'current_dots_out'):
                    self.current_dots_out = self.axis1.scatter(c_pokes_out['start-times'].values.astype(float), 
                            c_pokes_out['yaxis'].values.astype(float), 
                            color=c_pokes_out['colors'].values,
                            s=8, zorder=2, 
                            marker=self.get_current_settings.poke_markers['out']) 
                else:
                    self.current_dots_out.set_offsets(np.c_[c_pokes_out['start-times'].values.astype(float), c_pokes_out['yaxis'].values.astype(float)])
                    self.current_dots_out.set_color(c_pokes_out['colors'].values)
        # -------- Top plot settings: ------------
        self.axis1.spines['top'].set_visible(False)
        self.axis1.spines['right'].set_visible(False)
        self.axis1.set_yticks([self.processor.LAST_TRIAL])
        self.axis1.set_ylim([self.processor.LAST_TRIAL-1, self.processor.LAST_TRIAL+1])
        # ----------- Legend: -------------
        self.axis2.legend(loc='upper center', 
                bbox_to_anchor=(0.5, -0.2), 
                handles=self.legend, 
                ncol=4, 
                fontsize=9, 
                fancybox=True, 
                shadow=True)

    @log
    def get_legend(self):
        """
        Computes the appropriate legend for the settings of the session.
        """

        legend_elements = []
        for state in self.get_current_settings.state_list:
            entry = Line2D([0], [0], linewidth=5, color=self.get_current_settings.state_list[state], label=state)
            legend_elements.append(entry)
        self.legend = legend_elements

    @log
    def show(self, detached=False):
        """ Method used to show the windows """
        # Prevent the call to be recursive because of the mdi_area
        if not detached:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called
        else:
            BaseWidget.show(self)
        
        self._stop = False  # Flag used to close the gui in the middle of a loading
        self.read_message_queue()  # Read all the data and update the plot
        
        if not self._stop and self.session.is_running:  # If it is in real time...
            self._timer.start(conf.SESSIONRASTER_PLUGIN_REFRESH_RATE)

    def before_close_event(self):
        """ Method called before closing the windows. """
        self._timer.stop()
        self._stop = True
        self.session.sessionraster_action.setEnabled(True)
        self.session.sessionraster_detached_action.setEnabled(True)

    @property
    def mainwindow(self):
        return self.session.mainwindow

    @property
    def title(self):
        return BaseWidget.title.fget(self)

    @title.setter
    def title(self, value):
        BaseWidget.title.fset(self, value)

    @property
    def get_current_settings(self):
        return SimpleNamespace(**self.complete_settings[self._task_settings.value])
