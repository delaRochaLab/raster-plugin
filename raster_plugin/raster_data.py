import numpy as np
import pandas as pd
from pyforms import conf
import json
import os
from types import SimpleNamespace
from AnyQt.QtCore import QObject, pyqtSignal, pyqtSlot
from raster_plugin.exc_manager import log

class RasterData():
    """
    This class does the heavy-lifting on the data processing that's coming from the raster windows.
    It basically parses trials coming from BPOD in a way that makes them easy to plot in the scatter and bar plots of the
    main window. It has to compute four things: states and pokes in the already completed trials, and states and pokes
    in the current trial. The former can be aligned to a certain state defined in the settings, while the latter cannot
    (no alignment is possible if the trial hasn't finished, since we always align to the latest occurence of the alignment
    state). It stores all the data into the dataframes defined inside init_dataframes(), which is necessary if the user has
    to change the settings on the fly. The arrays are then processed, sliced and sent to the plot.
    """

    def __init__(self, axis_size: str, settings):
        self.offset = np.array([])
        # Dataframes:
        self.init_dataframes()
        self.st = settings
        # Plot variables:
        self.plot_mode = 'Last'
        self.start = 0
        self.LAST_TRIAL = 1
        self.PLOT_START = 0
        self.PLOT_END = 1
        self.axis_size = int(axis_size)

    def init_dataframes(self):
        self.states = pd.DataFrame()
        self.pokes = pd.DataFrame()
        self.current_trial = pd.DataFrame()
        self.c_pokes_dataframe = pd.DataFrame()

    def change_settings(self, settings):
        self.st = settings
        self.init_dataframes()
        self.LAST_TRIAL = 1

    def change_alignment(self, new_alignment: str):
        """
        To implement in the future, with a separate alignment control.
        """
        self.st.alignment = new_alignment 
        self.init_dataframes()
        self.LAST_TRIAL = 1

    @log
    def process_past_trials(self, trials, dataframe):
        """
        This method receives trials from the main window and obtains the states and pokes data by parsing them.
        It uses the STATE rows for the states (since these trials have already finished) and calls process_pokes()
        to parse the pokes.
        """
        # --------- STATES COMPUTATION -----------
        states = dataframe.query("TYPE=='STATE' and MSG in @self.st.state_list")
        state_names = states['MSG']
        grouper = states.groupby(pd.cut(states.index, bins=trials), as_index=False)
        length = states['BPOD-FINAL-TIME'].astype(float) - states['BPOD-INITIAL-TIME'].astype(float)
        states_dataframe = pd.DataFrame({'start-times': states['BPOD-INITIAL-TIME'], 
                                        'length': length, 
                                        'states': state_names, 
                                        'yaxis': grouper.ngroup() + self.LAST_TRIAL})
        # Offset calculation:
        self.offset = np.array([])
        indices = state_names[(state_names.shift(-1) != state_names) & (state_names==self.st.alignment)].index
        self.offset = states['BPOD-INITIAL-TIME'].loc[indices].values
        states_dataframe['offset'] = states_dataframe['start-times'].astype(float) - np.repeat(self.offset, grouper.size()).astype(float)
        # Colors:
        states_dataframe['colors'] = states_dataframe['states'].map(self.st.state_list)
        # --------- POKES COMPUTATION -----------
        pokes = dataframe.loc[(dataframe['+INFO'].isin(self.st.pokes_in) | dataframe['+INFO'].isin(self.st.pokes_out))]
        grouper = pokes.groupby(pd.cut(pokes.index, bins=trials), as_index=False)
        pokes_dataframe = pd.DataFrame({'start-times': pokes['BPOD-INITIAL-TIME'], 
                                        'pokes': pokes['+INFO'], 
                                        'yaxis': grouper.ngroup() + self.LAST_TRIAL}) 
        # Offset calculation:
        pokes_dataframe['offset'] = pokes_dataframe['start-times'].astype(float) - np.repeat(self.offset, grouper.size()).astype(float)
        # Colors:
        pokes_dataframe['colors'] = pokes_dataframe['pokes'].map({**self.st.pokes_in, **self.st.pokes_out}) 
        
        if self.LAST_TRIAL == 1:
            self.pokes, self.states = pokes_dataframe, states_dataframe
        else:
            self.pokes = self.pokes.append(pokes_dataframe)
            self.states = self.states.append(states_dataframe)

        self.current_trial = pd.DataFrame([])
        self.c_pokes_dataframe = pd.DataFrame([])
        self.LAST_TRIAL = self.states['yaxis'].iloc[-1] + 1     

    def process_current_trial(self, dataframe):
        """
        This method receives an icomplete piece of a trial and tries to get information about the states
        looking at the TRANSITION rows. Then it does the same wit hthe pokes, looking at the EVENTS rows.
        It oftentimes returns an empty structure, because the trial had just started and no information
        can yet be obtained.
        """
        # --------- STATES COMPUTATION -----------
        states = dataframe.query("TYPE=='TRANSITION'")
        transition_times = states['BPOD-INITIAL-TIME'].values.astype(float)
        if transition_times.size:
            lengths = transition_times - np.insert(transition_times, 0, 0)[:-1]
            transition_times = np.insert(transition_times, 0, 0)[:-1]
            states = np.insert(states['MSG'].values[:-1], 0, self.st.first_state)
            self.current_trial = pd.DataFrame({'start-times': transition_times, 
                                               'length': lengths,
                                               'states': states,
                                               'yaxis': self.LAST_TRIAL})
            self.current_trial = self.current_trial.query("states in @self.st.state_list")
            self.current_trial['colors'] = self.current_trial['states'].map(self.st.state_list)
        # --------- POKES COMPUTATION -----------
        pokes = dataframe.loc[(dataframe['+INFO'].isin(self.st.pokes_in) | dataframe['+INFO'].isin(self.st.pokes_out))]
        if pokes.size:
            self.c_pokes_dataframe = pd.DataFrame({'start-times': pokes['BPOD-INITIAL-TIME'], 
                                            'pokes': pokes['+INFO'],
                                            'yaxis': self.LAST_TRIAL}) 
            self.c_pokes_dataframe['colors'] = self.c_pokes_dataframe['pokes'].map({**self.st.pokes_in, **self.st.pokes_out})

    def check_plot_limits(self):
        """
        This method keeps track of the first and last trials that are shown in the plot, 
        which depend on the plot mode and the moment of the session. For the plot modes, 
        'Last' indicates that only the last N trials are shown, and the window has a scrolling behaviour;
        'First' indicates that the first trial in the session is the first trial in the plot;
        'Range' allows the user to specify the trial interval that will be shown.
        """
        if self.plot_mode == 'Last':
            self.PLOT_END = self.LAST_TRIAL
            if self.LAST_TRIAL > self.axis_size:
                self.PLOT_START = self.LAST_TRIAL - self.axis_size
            else:
                self.PLOT_START = 0
        elif self.plot_mode == 'Range':
            self.PLOT_START = self.start
            if self.LAST_TRIAL > self.axis_size:
                self.PLOT_END = self.start + self.axis_size
            else:
                self.PLOT_END = self.LAST_TRIAL
        else:
            self.PLOT_START = 0
            if self.LAST_TRIAL > self.axis_size:
                self.PLOT_END = self.axis_size
            else:
                self.PLOT_END = self.LAST_TRIAL

    def slice_dataframe(self, dataframe):
        """
        This method cuts a states or pokes dataframe from PLOT_START to the end;
        this cut dataframe is the one sent to the main window for plotting.
        """
        if dataframe.empty:
            start = 0
        else:
            start = dataframe['yaxis'].searchsorted(self.PLOT_START)[0]
        return dataframe[start:]        

    @property
    def past_states(self):
        return self.slice_dataframe(self.states)

    @property
    def past_pokes(self):
        return self.slice_dataframe(self.pokes)
    
    @property
    def current_states(self):
        return self.current_trial
    
    @property
    def current_pokes(self):
        return self.c_pokes_dataframe

